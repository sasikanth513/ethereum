import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { isChrome } from '../../helpers/functions.js';

import { Bets } from '../../../api/bets/bets.js';

Template.Bet_Page.onCreated(function appHomeOnCreated(){
  const tpl = this;
  tpl.dataDict = new ReactiveDict();

  tpl.subscribe('betInfo', FlowRouter.getParam('_id'));
});

Template.Bet_Page.helpers({
  betInfo: function(){
    let res = Bets.findOne({ uniqueId: FlowRouter.getParam("_id") });

    return res;
  },
  assets: function(){
    let assets = [ "BTC", "USD", "GBP" ];
    
    let res = Bets.findOne({ uniqueId: FlowRouter.getParam("_id") });

    if( res && res.better1Asset ){

      assets = assets.filter( item => item !== res.better1Asset )
      
    }

    return assets;

  }
});

Template.Bet_Page.onRendered(function appHomeOnRendered() {
  

});

Template.Bet_Page.events({
  'submit #place-bet-form': function (e, t) {
    e.preventDefault();

    let allGood = true;

    let obj = {};
    
    obj.name = t.$("#name").val();
    if( !obj.name || !obj.name.trim() || obj.name.length < 6){
      toastr.error("Name should be atleast 6 characters", "Error");
      allGood = false;
    }

    obj.email = t.$("#email").val();
    if( !obj.email || !obj.email.trim() || !validEmail(obj.email)){
      toastr.error("Invalid Email", "Error");
      allGood = false;
    }

    obj.asset = t.$("#asset").val();
    if( !obj.asset || !obj.asset.trim()){
      toastr.error("Please select asset", "Error");
      allGood = false;
    }



    if( allGood ){

      t.$(".submit-btn").addClass('loading');

      Meteor.call('placeBet', obj, FlowRouter.getParam('_id'), function (error, result) {
        t.$(".submit-btn").removeClass('loading');
        if(error){
          toastr.error(error.reason);
        }else{
          toastr.success("Bet Placed.", "Success");
          FlowRouter.go("/");
        }
      });
    }

  }
});

Template.Bet_Page.onDestroyed(() => {

});
