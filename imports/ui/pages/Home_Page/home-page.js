import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { isChrome } from '../../helpers/functions.js';

import { Bets } from '../../../api/bets/bets.js';

Template.Home_Page.onCreated(function appHomeOnCreated(){
  const tpl = this;
  tpl.dataDict = new ReactiveDict();

  tpl.subscribe('openBets');
});

Template.Home_Page.helpers({
  openBets() {
    const bets = Bets.find().fetch();
    return bets;
  }
});

Template.Home_Page.onRendered(function appHomeOnRendered() {
  if( !isChrome() ){
    $('.dimmer').dimmer('show');
  }

  let today = new Date();
  
  let startTime = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));

  //rounding to nearest hour
  startTime.setHours(startTime.getHours() + Math.round(startTime.getMinutes()/60));
  startTime.setMinutes(0);


  $('#startTime').calendar({
    disableMinute: true,
    minDate: startTime
  });

  $('#endTime').calendar({
    disableMinute: true,
    minDate: new Date( startTime.getTime() + (24 * 60 * 60 * 1000) )
  });

});

Template.Home_Page.events({
  'submit #place-bet-form': function (e, t) {
    e.preventDefault();

    let allGood = true;

    let obj = {};
    
    obj.name = t.$("#name").val();
    if( !obj.name || !obj.name.trim() || obj.name.length < 6){
      toastr.error("Name should be atleast 6 characters", "Error");
      allGood = false;
    }

    obj.email = t.$("#email").val();
    if( !obj.email || !obj.email.trim() || !validEmail(obj.email)){
      toastr.error("Invalid Email", "Error");
      allGood = false;
    }

    obj.asset = t.$("#asset").val();
    if( !obj.asset || !obj.asset.trim()){
      toastr.error("Please select asset", "Error");
      allGood = false;
    }

    obj.start = t.$('#startTime').calendar('get date');
    if( !obj.start){
      toastr.error("Please select start time", "Error");
      allGood = false;
    }

    obj.end = t.$('#endTime').calendar('get date');
    if( !obj.end){
      toastr.error("Please select end time", "Error");
      allGood = false;
    }

    obj.amount = t.$('#amount').val();
    if( !obj.amount || !obj.amount.trim()){
      toastr.error("Please enter amount", "Error");
      allGood = false;
    }

    // check end time is greater than start time

    if( obj.start && obj.end ){

      if( obj.start.getTime() >= obj.end.getTime() ){
        toastr.error("End time should be after start time", "Error");
        allGood = false;
      }

    }

    if( allGood ){

      t.$(".submit-btn").addClass('loading');

      Meteor.call('bets.create', obj, function (error, result) {
        t.$(".submit-btn").removeClass('loading');
        if(error){
          toastr.error(error.reason);
        }else{
          toastr.success("Bet Placed.", "Success");
          FlowRouter.go("/");
        }
      });
    }

  }
});

Template.Home_Page.onDestroyed(() => {

});
