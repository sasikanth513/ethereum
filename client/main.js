import '/imports/startup/client';
import '/imports/startup/both';
import '/imports/api/bets';
import '/imports/ui/layouts';

// pages
import '/imports/ui/pages/Home_Page';
import '/imports/ui/pages/Bet_Page';


import '/imports/ui/stylesheets/style.css';


import '/imports/ui/helpers';

import '/imports/plugins/semantic-calendar';
