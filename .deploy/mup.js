module.exports = {
  servers: {
    one: {
      host: '45.55.79.132',
      username: 'root',
      // pem:
      password:'sasikanth'
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    dockerImage: 'abernix/meteord:base',
    name: 'Ethereum',
    path: '../',
    servers: {
      one: {
        host: '45.55.79.132',
        username: 'root',
        // pem:
        password:'sasikanth'
      }
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      MONGO_URL: 'mongodb://localhost/meteor'
    },

    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 60
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};